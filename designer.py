# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'desginer.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import sys

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(690, 314)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.start_button = QtGui.QPushButton(self.centralwidget)
        self.start_button.setGeometry(QtCore.QRect(190, 140, 79, 25))
        self.start_button.setObjectName(_fromUtf8("start_button"))
        self.url_text = QtGui.QLineEdit(self.centralwidget)
        self.url_text.setGeometry(QtCore.QRect(240, 10, 171, 21))
        self.url_text.setObjectName(_fromUtf8("url_text"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(210, 10, 57, 15))
        self.label.setObjectName(_fromUtf8("label"))
        self.user_text = QtGui.QLineEdit(self.centralwidget)
        self.user_text.setGeometry(QtCore.QRect(240, 50, 171, 21))
        self.user_text.setObjectName(_fromUtf8("user_text"))
        self.pwd_text = QtGui.QLineEdit(self.centralwidget)
        self.pwd_text.setGeometry(QtCore.QRect(240, 90, 171, 21))
        self.pwd_text.setObjectName(_fromUtf8("pwd_text"))
        self.label_2 = QtGui.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(190, 50, 57, 15))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(200, 90, 57, 15))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.quit_button = QtGui.QPushButton(self.centralwidget)
        self.quit_button.setGeometry(QtCore.QRect(360, 140, 79, 25))
        self.quit_button.setObjectName(_fromUtf8("quit_button"))
        self.quit_button.clicked.connect(self.close_app)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 690, 20))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuArquivo = QtGui.QMenu(self.menubar)
        self.menuArquivo.setObjectName(_fromUtf8("menuArquivo"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionSave = QtGui.QAction(MainWindow)
        self.actionSave.setObjectName(_fromUtf8("actionSave"))
        self.actionQuit = QtGui.QAction(MainWindow)
        self.actionQuit.setObjectName(_fromUtf8("actionQuit"))
        self.actionQuit.triggered.connect(self.close_app)
        self.menuArquivo.addAction(self.actionSave)
        self.menuArquivo.addAction(self.actionQuit)
        self.menubar.addAction(self.menuArquivo.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "CnabSender", None))
        self.start_button.setText(_translate("MainWindow", "Start", None))
        self.label.setText(_translate("MainWindow", "URL", None))
        self.label_2.setText(_translate("MainWindow", "Usuario", None))
        self.label_3.setText(_translate("MainWindow", "Senha", None))
        self.quit_button.setText(_translate("MainWindow", "Quit", None))
        self.menuArquivo.setTitle(_translate("MainWindow", "Arquivo", None))
        self.actionSave.setText(_translate("MainWindow", "Save", None))
        self.actionQuit.setText(_translate("MainWindow", "Quit", None))

    def close_app(self):
        sys.exit()

    #def communicate_to_server(self):
        
if __name__ == "__main__":    
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

