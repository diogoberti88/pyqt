import sys
from PyQt4 import QtGui, QtCore
import requests

class Window(QtGui.QMainWindow):

	def __init__(self):
		super(Window,self).__init__()
		self.setGeometry(100,100,500,400)
		self.setWindowTitle('Cnab')
		#Precisa de um png na pasta e referenciar o path para mostrar o icon
		#self.setWindowIcon(QtGui.QIcon(''))

		#Cria uma acao para ser chamada no menu superior
		extractAction = QtGui.QAction("&Get To The Chopper!", self)
		extractAction.setShortcut("Ctrl+Q")
		extractAction.setStatusTip("Leave the App")
		extractAction.triggered.connect(self.close_application)
		#Cria a status bar para mostrar o status Tip
		self.statusBar()
		#Cria um Menubar superior
		mainMenu = self.menuBar()
		#Adiciona o Menu File na barra de menu
		fileMenu = mainMenu.addMenu('&File')
		#Adiciona a Acao
		fileMenu.addAction(extractAction)
		extractActionSave = QtGui.QAction('Salvar',self)
		extractActionSave.triggered.connect(self.save_app)
		self.toolbar = self.addToolBar("Extraction")
		self.toolbar.addAction(extractActionSave)


		self.home()

	def home(self):
		btn = QtGui.QPushButton("Quit", self)
		btn.clicked.connect(self.close_application)	
		btn.resize(btn.sizeHint())
		btn.move(100,100)

		checkbox = QtGui.QCheckBox("Enlarge Window", self)
		checkbox.move(100,200)
		checkbox.resize(btn.sizeHint())
		checkbox.stateChanged.connect(self.enlarge_window)
		

		self.show()

	def enlarge_window(self, state):
		if state == QtCore.Qt.Checked:
			self.setGeometry(50,50,670,450)
		else:
			self.setGeometry(50,50,500,400)


	def close_application(self):
		choice = QtGui.QMessageBox.question(self,
			'Saindo',
			"Tem certeza que deseja Fechar?",
			QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
		if choice == QtGui.QMessageBox.Yes:
			print("Closing")
			sys.exit()
		else:
			print("Not Closing")

	def save_app(self):
		print("Salvando")


def run():

	app = QtGui.QApplication(sys.argv)
	GUI = Window()

	sys.exit(app.exec_())

run()